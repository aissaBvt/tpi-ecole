﻿/**
* Projet : Virtual auto guide (application Unity)
* Classe : TrafficLightManager
* Description : Gestion des feux sur la carte
* Auteur : Aïssa Bovet
* Date : 04.06.2018
* Version : 1.0
*/
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrafficLightManager:MonoBehaviour
{
    //List of the "north" Traffic lights
    private List<TrafficLight> northLights;
    //List of "south" Traffic lights
    private List<TrafficLight> southLights;
    //List of "east" Traffic lights
    private List<TrafficLight> eastLights;
    //List of "west" Traffic lights
    private List<TrafficLight> westLights;
    //List of all traffic lights
    private List<List<TrafficLight>> allLights;

    //Default time between state switch
    private const int TIMEBETWEENCYCLEDEFAULT = 3;
    //Time it take the fire to change color
    private float time;
    //Time left until change
    private float timer;
    //State for all Fire
    private bool active;
    //Time to let the car already in the carefour go
    private const int DEBUFBETWEENCHANGE = 2;
    //The fire that is curently active
    private int idActiveFire;
    /// <summary>
    /// Get/Set for _time
    /// </summary>
    public float Time
    {
        get
        {
            return time;
        }

        set
        {
            time = value;
        }
    }
    /// <summary>
    /// Get/Set for _timer
    /// </summary>
    public float Timer
    {
        get
        {
            return timer;
        }

        set
        {
            timer = value;
        }
    }

    public int DEBUF
    {
        get
        {
            return DEBUFBETWEENCHANGE;
        }
    }

    /// <summary>
    /// When the object is created
    /// the time it take to change is set to the default time
    /// The timer is set on the time
    /// and fire are not active
    /// </summary>
    private void Start()
    {
        Time = TIMEBETWEENCYCLEDEFAULT;
        Timer = Time;
        active = false;
        idActiveFire = -1;
    }
    /// <summary>
    /// Set both lane of fire on opposite state and launch the coroutine to change state on each one
    /// according to the time
    /// </summary>
    public void StartTheLights()
    {
        //Stop the previous timer if one is actually running
        StopAllCoroutines();
        //Initialisations of the lists
        northLights = new List<TrafficLight>();
        southLights= new List<TrafficLight>();
        westLights = new List<TrafficLight>();
        eastLights = new List<TrafficLight>();
        allLights = new List<List<TrafficLight>>();
        //retriving all the traffic lights
        RetriveAllLights();
        //Set the color of all "North" on true
        foreach (var item in northLights)
        {
            item.SetColor(true);
        }
        //The traffics light become active
        active = true;
        //Launch the coroutine that will periodicaly change the lights
        StartCoroutine(TimerChange());
    }
    /// <summary>
    /// Retrive all Traffic light and assign them to their list
    /// </summary>
    public void RetriveAllLights()
    {
        //Find all object with the tag NorthSouth and add them to the "Vertical" list
        foreach (GameObject tL in GameObject.FindGameObjectsWithTag("North"))
        {
            northLights.Add(tL.GetComponent<TrafficLight>());
        }
        foreach (GameObject tL in GameObject.FindGameObjectsWithTag("South"))
        {
            southLights.Add(tL.GetComponent<TrafficLight>());
        }
        //Find all object with the tag EastWest and add them to the "Horizontal" list
        foreach (GameObject tL in GameObject.FindGameObjectsWithTag("East"))
        {
            eastLights.Add(tL.GetComponent<TrafficLight>());
        }
        foreach (GameObject tL in GameObject.FindGameObjectsWithTag("West"))
        {
            westLights.Add(tL.GetComponent<TrafficLight>());
        }
        allLights.Add(northLights);
        allLights.Add(southLights);
        allLights.Add(eastLights);
        allLights.Add(westLights);
    }
    /// <summary>
    /// Empty all the lists
    /// </summary>
    public void RemoveAllLights() {
        northLights=new List<TrafficLight>();
        southLights = new List<TrafficLight>();
        eastLights = new List<TrafficLight>();
        westLights=new List<TrafficLight>();
        allLights = new List<List<TrafficLight>>();
    }
    /// <summary>
    /// Change the light to the traffic light who should be on to green
    /// </summary>
    public void SetAFireGreen()
    {
        idActiveFire++;
        switch (idActiveFire)
        {
            case 0:
                foreach (TrafficLight lights in northLights)
                {
                    lights.SetColor(true);
                }
                break;
            case 1:
                foreach (TrafficLight lights in southLights)
                {
                    lights.SetColor(true);
                }
                break;
            case 2:
                foreach (TrafficLight lights in westLights)
                {
                    lights.SetColor(true);
                }
                break;
            case 3:
                foreach (TrafficLight lights in eastLights)
                {
                    lights.SetColor(true);
                    idActiveFire = -1;
                }
                break;
        }
        
    }
    /// <summary>
    /// change all fire to red
    /// </summary>
    private void SetAllFiresRed()
    {
        //Change all Fire to red
        foreach (var fires in allLights)
        {
            foreach (var lights in fires)
            {
                lights.SetColor(false);
            }

        }
    }
    /// <summary>
    /// Coroutine changing the light every second in the Time variable
    /// Start the countdown at each iteration
    /// </summary>
    /// <returns></returns>
    IEnumerator TimerChange()
    {
        while (active)
        {
            SetAllFiresRed();
            yield return new WaitForSecondsRealtime(DEBUF);
            //Change all lights
            SetAFireGreen();
            //Start the countdown
            StartCoroutine(CountDown());
            //Wait Time second before the next iteration
            yield return new WaitForSeconds(Time);
            
        }
        
    }
    /// <summary>
    /// Coroutine countdown of the time until the light change
    /// </summary>
    /// <returns></returns>
    IEnumerator CountDown()
    {
        for (int i = Convert.ToInt32(Time)+DEBUF; i >0 ; i--)
        {
            Timer = i;
            yield return new WaitForSeconds(1);
        }
        StopCoroutine(CountDown());
    }
    /// <summary>
    /// Change the time between the change
    /// </summary>
    /// <param name="newTime"></param>
    public void ChangeTime(float newTime) {
        Time = newTime;
    }
   
}