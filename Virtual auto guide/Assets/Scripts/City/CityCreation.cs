﻿/**
* Projet : Virtual auto guide (application Unity)
* Classe : CityCreation.cs
* Description : Creation and managing the city
* Auteur : Aïssa Bovet
* Date : 04.06.2018
* Version : 1.0
*/
using System;
using System.Collections.Generic;
using UnityEngine;

public class CityCreation:MonoBehaviour
{
    //Size of the city (square sizexsize)
    [SerializeField]
    private int size;
    //The gameObject of a tile to create the town
    [SerializeField]
    private GameObject tile;
    // The manger of the traffic lights in the scenes
    private TrafficLightManager tlm;
    // The object city to parent all the tiles
    private GameObject cityObject;
    // all angle at wich the tiles can be turned
    private int[] angles= {0,90,180};
    //The minimap camera
    [SerializeField]
    private GameObject minimapCamera;
    private const int DEFAULTSIZE = 10;
    /// <summary>
    /// Create a city with a size of 10
    /// </summary>
    private void Start()
    {
        CreateCity(DEFAULTSIZE);
    }
    /// <summary>
    /// Create a city 
    /// </summary>
    /// <param name="size"></param>
    public void CreateCity(int size)
    {
        minimapCamera.transform.position = new Vector3((size/2.5f)*10,size*10,(size/ 2.5f) *10);
        //Instantiate the first tile of the city
        GameObject city = Instantiate(tile);
        //Name it city
        city.name = "City";
        //Set the tile on (0,0,0)
        city.transform.position = new Vector3(0, 0, 0);
        //For the lenght and the width of the city
        for (int i = 0; i < size; i++)
        {
            for (int j = 0; j < size; j++)
            {
                //if the tile is  on the first iteration ignore it
                //(the first Tile is created out of the boucle so it can be the parent to all next tiles)
                if (i==0 && j==0)
                {
                    
                }
                else
                {
                    //Create a Tile
                    GameObject newTile = Instantiate(tile, city.transform);
                    //Set the position of the tile (10 is the size of the tile used in the project)
                    newTile.transform.position = new Vector3(i * 10, 0, j * 10);
                    //Rotate the tile to one of the angle possible to avoid having all vertical or all horizontal traffic light at the same state
                    newTile.transform.Rotate(Vector3.up *angles[UnityEngine.Random.Range(0,2)]);
                }
                
            }
        }
        //Add the traffic light manager to the city
        city.AddComponent<TrafficLightManager>();
        //set the variable of the traffic manager to the just added
        tlm = city.GetComponent<TrafficLightManager>();
        //The city is assigned to the city object , so it can be used elswhere in the class
        cityObject = city;
    }
    /// <summary>
    /// Destroy the city after stopping the traffics light and reseting the traffic lights list
    /// </summary>
    public void DestroyCity()
    {   
        //Stop the timer changing the state of the traffics lights
        tlm.StopAllCoroutines();
        //reset the traffic lights lists
        tlm.RemoveAllLights();
        //Destroy the city
        Destroy(cityObject);
    }
    /// <summary>
    /// Start the simulation by starting the cycle of the traffics lights
    /// </summary>
    public void StartSim() {
        tlm.StartTheLights();
    }
    /// <summary>
    /// Change the time for fires to change state
    /// </summary>
    /// <param name="time"></param>
    public void ChangeFireTime(float time) {
        tlm.ChangeTime(time);
    }
    /// <summary>
    /// Return the time left from the traffic light manager to the U.I
    /// </summary>
    /// <returns></returns>
    public float TimeLeft() {
        return tlm.Timer;
    }
}