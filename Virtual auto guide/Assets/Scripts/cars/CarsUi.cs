﻿/**
* Projet : Virtual auto guide (application Unity)
* Classe : CarUI.cs
* Description : Manage user interface on the car demo scene
* Auteur : Aïssa Bovet
* Date : 11.06.2018
* Version : 1.0
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CarsUi : MonoBehaviour {
    //Label in the editor for the car speed test
    [Header("Voiture speed test")]
    //The wall gameObject
    [SerializeField]
    private GameObject wall;
    //If the wall should be moving
    private bool wallrunAway;
    //Label in the editor for the self driving car
    [Header("SelfDrivingCar")]
    //The wall that block the self-driving car gameObject
    [SerializeField]
    private GameObject[] invisibleWall;
    //label in the editor for the camera
    [Header("Cameras")]
    //The camera for the car speed test
    [SerializeField]
    private GameObject camVoitureSpeedTest;
    //The camera for the self driving car
    [SerializeField]
    private GameObject camselfDrivingCar;
    //Set the wall to no move and the camera on the speed test
    private void Start()
    {
        wallrunAway = false;
        camVoitureSpeedTest.SetActive(true);
        camselfDrivingCar.SetActive(false);
    }
    /// <summary>
    /// Move the wall of the speed test if it should move
    /// </summary>
    private void Update()
    {
        //Check if the wall should be moving
        if (wallrunAway)
        {
            //Move the wall
            wall.transform.Translate(-0.03f, 0, 0);
        }
    }
    /// <summary>
    /// Change the variable if the wall should move
    /// </summary>
    public void MakeTheWallMove() {
        wallrunAway = !wallrunAway;
    }
    /// <summary>
    /// Change between the two camera
    /// </summary>
    public void SwitchCameras() {
        //Each camera is activated/deactivated opposing it's current status
        camVoitureSpeedTest.SetActive(!camVoitureSpeedTest.activeSelf);
          camselfDrivingCar.SetActive(!camselfDrivingCar.activeSelf);
    }
    /// <summary>
    /// Go back to the main menu
    /// </summary>
    public void BackToMainMenu() {
        SceneManager.LoadScene(0);
    }
}
