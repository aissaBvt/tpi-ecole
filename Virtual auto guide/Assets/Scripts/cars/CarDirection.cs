﻿/**
* Projet : Virtual auto guide (application Unity)
* Classe : CarDirection.cs
* Description : Manage the main car orientation and turns
* Auteur : Aïssa Bovet
* Date : 04.06.2018
* Version : 1.0
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarDirection : MonoBehaviour {
    //The target the car must follow
    private GameObject target;
    //The car's right sensor
    private Sensor rSensor;
    //The car's left sensor
    private Sensor lSensor;
    //The car's rigidbody
    private Rigidbody rb;
    //The direction of the car
    private float carAngle;
    //The limit of the rotation the car must do
    private float carAngleLimit;
    //The states of the car
    private enum states{forward,right,left};
    //The car state
    private states state;
    //The step the car mus3t do on it's rotation
    private float step;
    //The power of the ratio wich delimit of how long the virage is 
    private const float POWLEFT=1.7855f;
    private float POWRIGHT = 1.766f;
    private Vector3 initialPosition;
    private Vector3 initialRotation;
    //Pause the sensor from mesuring 
    private bool AreSensorsPaused;
    /// <summary>
    /// Initialise all values
    /// </summary>
    private void Start () {
        //Get the target GameObject
        target = GameObject.Find("Target");
        //Get the rigidBody of the car
        rb = GetComponent<Rigidbody>();
        //Get the right sensor of the car
        rSensor = transform.GetChild(3).GetComponent<Sensor>();
        //Get the left Sensor of the car
        lSensor = transform.GetChild(4).GetComponent<Sensor>();
        //Initialise the state of the car
        state = states.forward;
        //The sensors are not paused
        AreSensorsPaused = false;
        //Set the speed of car to 1
        GetComponent<Car>().Speed=1;
        initialPosition = transform.position;
        initialRotation = transform.rotation.eulerAngles;
    }
    /// <summary>
    /// check the state and go forward or turn in either direction if necessery
    /// </summary>
    private void FixedUpdate()
    {
        //Check the state
        switch (state)
        {
            //The car must go forward
            case states.forward:
                //If the sensor are paused ignore the turns
                if (!AreSensorsPaused)
                {
                    //Get the direction of the target
                    int direction = WherIsMyTarget();
                    //If the target is on the left and the left sensor detect, and the object detected is a turn pillar
                    if (direction == 0  && lSensor.Turn)
                    {
                        //Get the curent rotation of the car
                        carAngle = transform.eulerAngles.y;
                        //Set the limit at -90 of the car current orientation
                        carAngleLimit = carAngle - 90;
                        //Pause the sensor so the other pillar don't distrub the car
                        StartCoroutine(SensorPause());
                        //Change the state of the car to left
                        state = states.left;
                    }
                    //If the target is on the right and the obstacle detected is a pillar
                    if (direction == 1  && rSensor.Turn)
                    {
                        //Get the curent rotation of the car
                        carAngle = transform.eulerAngles.y;
                        //Take the limit of the angle to wich the car should turn
                        carAngleLimit = carAngle + 90;
                        //Pause the sensor
                        StartCoroutine(SensorPause());
                        //Change the state of the car to right
                        state = states.right;
                    }
                }
                break;
                //When the car should turn right
            case states.right:
                //While the car orientation is less than the limit
                if (carAngle < carAngleLimit)
                {
                    //Get the rayon from the pillar
                    float rayon = Vector3.Distance(transform.position, rSensor.TurnPillar.transform.position);
                    //Calculate the stesp to turn
                    step = ArcCalcul(rayon);
                    //The car turn by the step given
                    carAngle += (step * Mathf.Pow(10, -POWRIGHT));//* Time.fixedTime;
                    rb.rotation = Quaternion.Euler(carAngle * Vector3.up);
                }
                else 
                {
                    //Set the rotation at the wanted roatation
                    rb.rotation = Quaternion.Euler(carAngleLimit * Vector3.up);
                    //When no more obstacle are detected by the right sensor change state
                    if (!rSensor.ObstacleDetected)
                        state = states.forward;
                }
                break;
            case states.left:
                if (carAngle>carAngleLimit)
                {
                    //Get the distance between the car and the pillar
                    float rayon = Vector3.Distance(transform.position, lSensor.TurnPillar.transform.position);
                    //Calculate the step at wich the car must rotate
                    step = ArcCalcul(rayon);
                    carAngle -= (step * Mathf.Pow(10, -POWLEFT)) ;
                    //The car turn by the step given
                    rb.rotation = Quaternion.Euler(carAngle * Vector3.up);
                    
                }
                else 
                {
                    //Set the rotation at the wanted roatation
                    rb.rotation = Quaternion.Euler(carAngleLimit * Vector3.up);
                    //When no more obstacle are detected by the left sensor change state
                    if (!lSensor.ObstacleDetected)
                        state = states.forward;
                }
                break;
            default:
                break;
        }
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            transform.position = initialPosition;
            transform.rotation = Quaternion.Euler(initialRotation);
        }
    }
    /// <summary>
    /// Return the direction in wich the target is
    /// </summary>
    /// </summary>
    /// <returns></returns>
    private int WherIsMyTarget() {
        Vector3 relativePoint = transform.InverseTransformPoint(target.transform.position);
        //Check if the target is to the left
        if (relativePoint.x < 0.0)
        {
            return 0;
        }
        //Check if the target is to the right
        else if (relativePoint.x > 0.0)
        {
            return 1;
        }
        //if the target is neither it's in front
        else
        {
            return 2;
        }
    }
    /// <summary>
    /// Calcul the steps of rotation the gameObject must take in the virage
    /// </summary>
    /// <param name="rayon"></param>
    /// <returns></returns>
    private float ArcCalcul(float rayon) {
        //Get a quart of the circle's perimeter
        float distQuartCercle = (2 * Mathf.PI * rayon) / 4;
        //Get the speed of the car
        float carSpeed = GetComponent<Car>().Speed;
        //Get the time the car will take to complete the distance
        float rotationTime = distQuartCercle / carSpeed;
        //Get the step of each rotation
        float step = 90/rotationTime;
        return step;
    }
    /// <summary>
    /// Pause the sensor the same turn is not taken twice
    /// </summary>
    /// <returns></returns>
    private IEnumerator SensorPause() {
        //Pause the sensors 
        AreSensorsPaused = true;
        for (int i = 0; i < 2; i++)
        {
            yield return new WaitForSeconds(1);
        }
        AreSensorsPaused = false;
    }
    
    

}
