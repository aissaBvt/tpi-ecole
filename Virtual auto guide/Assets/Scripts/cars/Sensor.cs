﻿/**
* Projet : Virtual auto guide (application Unity)
* Classe : Sensor.cs
* Description : Sensors of the car, detecting obstacles
* Auteur : Aïssa Bovet
* Date : 11.06.2018
* Version : 1.0
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sensor : MonoBehaviour {
    //Distance to the obstacle
    float distance;
    //if an obstacle is detected
    bool obstacleDetected;
    //if the obstacle is for turning
    bool turn;
    //The gameObject pillar
    private GameObject turnPillar;
    //The rays cast that will be send to detect obstacles
    private RaycastHit hit;
    //Get set of the distance
    public float Distance
    {
        get
        {
            return distance;
        }

        set
        {
            distance = value;
        }
    }
    //Get set if a obstacle is detected
    public bool ObstacleDetected
    {
        get
        {
            return obstacleDetected;
        }

        set
        {
            obstacleDetected = value;
        }
    }
    //Get set if the obstacle is for turning
    public bool Turn
    {
        get
        {
            return turn;
        }

        set
        {
            turn = value;
        }
    }
    //Get set of the turn Pillar
    public GameObject TurnPillar
    {
        get
        {
            return turnPillar;
        }

        set
        {
            turnPillar = value;
        }
    }
    /// <summary>
    /// Draw a ray that will detect obstacles and if those obstacles are for turning
    /// </summary>
    void Update () {
        //Send a ray in the given direction
        if (Physics.Raycast(gameObject.transform.position, transform.forward/2, out hit)){
            //Draw the ray to be seen in the editor at runtime
            Debug.DrawRay(gameObject.transform.position, transform.forward, Color.yellow);
            //If the distance is less than 10 M (1unit = 10m)
            if (hit.distance<=1)
            {
                //Change the obstacle detected to true
                ObstacleDetected = true;
                //Set the distance to the new distance
                Distance = hit.distance;
                //If the gameObject collider is one the turning pillar 
                if (hit.collider.tag=="Turn")
                {
                    //Change the turn to true
                    Turn = true;
                    //Set the gameObject turn pillar is the collider
                    TurnPillar = hit.collider.gameObject;
                }
                else
                {
                    //If the gameobject is not a turn pillar the turn is set on false
                    Turn = false;
                }
            }
            else
            {
                //If the distance is greater than one set all obstacle's bool to false
                ObstacleDetected = false;
                Turn = false;
            }
        }
        else
        {
            //If nothing is detected (infinite ray without obstacles)
            ObstacleDetected = false;
            Turn = false;
        }
    }
}
